(function() {
  'use strict';

  angular
    .module('taptoemail', [])
    .controller('taptoemailController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location'];

  function loadFunction($scope, structureService, $location) {
    structureService.registerModule($location, $scope, 'taptoemail');
    $scope.email = $scope.taptoemail.modulescope.email;
    $scope.buttonType = $scope.taptoemail.modulescope.buttonType;
    $scope.caption = $scope.taptoemail.modulescope.caption;
    $scope.position = $scope.taptoemail.modulescope.position;
    $scope.goToLink = function(data){
     window.open(data, '_system');
    }
  }
}());
