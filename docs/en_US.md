# **(How to) Configure the Tap to Email module**

This module has 4 configurable fields:

| Field  | Value  | Description  |   
|---|---|---|---|---|
| Email  | Phone Number  | Email to be used  |
| Button text  | string  |  Text to be displayed in the button |
| Button type  | Standard / Big  | Button type  |
| Button position  | Left / Center / Right  | Button position  |
