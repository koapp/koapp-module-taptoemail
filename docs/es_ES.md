# **(How to) Configurar el módulo Tap to Email**

Este módulo tiene 4 campos configurables:

| Campo  | Valores  | Descripción  |   
|---|---|---|---|---|
| Email  | Email  | Email a utilizar  |
| Button text  | string  |  Texto que quieres que se muestre en el botón |
| Button type  | Standard / Big  | Tipo de botón  |
| Button position  | Left / Center / Right  | Posición del botón  |
